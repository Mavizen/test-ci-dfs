# Objectif
L'objectif de ce template est de fournir une structure cohérente et automatisée pour les pipelines CI/CD, permettant ainsi d'accélérer le développement et de garantir la qualité du code.

# Contenu
Ce template contient les éléments suivants :

* ruff.yaml: Configuration pour la qualité du code Python.
* build.yaml: Configuration pour la construction de l'image Docker.
* analyze.yaml: Configuration pour l'analyse statique de l'image Docker.
* push-dev.yaml: Configuration pour pousser l'image Docker vers un environnement de développement.
* push-prod.yaml: Configuration pour pousser l'image Docker vers un environnement de production.

 # Intégration dans un projet existant
Pour intégrer ce template dans un projet existant, suivez ces étapes :

Modifiez votre fichier .gitlab-ci.yml en incluant le template depuis une URL distante :
yaml

```yaml
include: "https://gitlab.local.cesbron.fr/template/ci-files/raw/main/default-gitlab-ci.yaml"

stages:
  - quality-check
  - build
  - test
  - release

# Vos autres configurations de pipeline peuvent être ajoutées ici
```

Personnalisez les variables selon les besoins de votre projet en modifiant le fichier .gitlab-ci.yml ou en définissant des variables d'environnement dans les paramètres du pipeline sur GitLab.

# Remplacement des variables
Ce template utilise des variables pour paramétrer certains aspects du pipeline. Vous pouvez remplacer ces variables en définissant vos propres valeurs. Voici un exemple de remplacement de variables :

```yaml
variables:
  CONTAINER_TEST_IMAGE_TAG: dev | "votre_tag_dev"
  CONTAINER_RELEASE_IMAGE_TAG: latest | "votre_tag_prod"
  
  DOCKER_USE: yes | no
  DOCKER_ANALYZE: yes | no
  DOCKER_PUSH_IMAGE: yes | no
  
  PYTHON_USE: yes | no
  PYTHON_VERSION: 3.9 | "votre_version_python"
```

:warning: Assurez-vous d'avoir créé un access token dans votre projet pour écrire sur le registry interne si la variable DOCKER_PUSH_IMAGE est définie à "yes".

# Accédez aux paramètres de votre projet sur GitLab.

Si vous êtes "Owner" du projet.
* Dans la partie "Settings / Access Tokens" → Créer un access token avec les droits de lecture / écriture sur la registry
* Dans la partie "Settings / CI/CD" → Créer les variables 
  * **CI_REGISTRY_PASSWORD** → à compléter avec le password de l'access token créé précédemment 
  * **CI_REGISTRY_USER** → à complter avec le nom de l'access token créé précédemment

# Exemple

````yaml
include: "https://gitlab.local.cesbron.fr/template/ci-files/raw/main/default-gitlab-ci.yaml"

stages:
  - quality-check
  - build
  - test
  - release

variables:
  CONTAINER_TEST_IMAGE_TAG: dev
  CONTAINER_RELEASE_IMAGE_TAG: release
  
  DOCKER_USE: yes
  DOCKER_ANALYZE: yes
  DOCKER_PUSH_IMAGE: yes
  
  PYTHON_USE: yes
  PYTHON_VERSION: 3.9
````